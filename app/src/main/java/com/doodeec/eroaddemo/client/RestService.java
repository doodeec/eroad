package com.doodeec.eroaddemo.client;

import com.doodeec.eroaddemo.client.response.PlaceholderResponse;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * @author Dusan Bartos
 */
public interface RestService {
    @GET("/api")
    void getSomething(Callback<PlaceholderResponse> callback);
}
