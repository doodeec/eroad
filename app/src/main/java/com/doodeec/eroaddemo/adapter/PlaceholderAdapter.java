package com.doodeec.eroaddemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.doodeec.eroaddemo.R;
import com.doodeec.eroaddemo.model.PlaceholderModel;
import com.doodeec.eroaddemo.view.PlaceholderViewHolder;

import java.util.List;

/**
 * @author Dusan Bartos
 */
public class PlaceholderAdapter extends RecyclerView.Adapter<PlaceholderViewHolder> {

    private Context mContext;
    private List<PlaceholderModel> mItems;

    public PlaceholderAdapter(Context context) {
        mContext = context;
    }

    public void setItems(List<PlaceholderModel> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public PlaceholderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlaceholderViewHolder(
                LayoutInflater.from(mContext).inflate(R.layout.holder_placeholder, parent, false));
    }

    @Override
    public void onBindViewHolder(PlaceholderViewHolder holder, int position) {
        holder.setName("Viewholder name");
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
